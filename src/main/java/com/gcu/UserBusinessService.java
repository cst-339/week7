package com.gcu;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserBusinessService {
	@Autowired 
	UserRepository service;
	
	public UserBusinessService(UserRepository usersRepository) {
		this.service = usersRepository;
	}
	
	public List<UserModel> getAllUsers(){
		List<UserEntity> usersEntity = service.findAll();
		List<UserModel> usersDomain = new ArrayList<UserModel>();
		for (UserEntity user : usersEntity) {
			usersDomain.add(new UserModel(user.getId(), user.getUsername(), user.getPassword()));
		}
		return usersDomain;
	}
}
