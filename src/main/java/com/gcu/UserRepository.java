package com.gcu;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.gcu.UserEntity;

public interface UserRepository extends MongoRepository<UserEntity, String> {
	UserEntity findByUsername(String username);
}
